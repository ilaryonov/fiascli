package models

import (
	_"fiascli/config"
)

type Option struct {
	Id int
	Name string
	Value string
}

func(o *Option) GetValueByName() string {
	return o.Name
}