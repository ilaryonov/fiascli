package models

import (
	"github.com/jinzhu/gorm"
	"path/filepath"
	"sync"
	"fmt"
	"os"
	"encoding/xml"
	"fiascli/config"
	"time"
)

type AddrObject struct {
	gorm.Model
	Aoid string `xml:"AOID,attr"`
	Aoguid string `xml:"AOGUID,attr"`
	Parentguid string `xml:"PARENTGUID,attr"`
	Formalname string `xml:"FORMALNAME,attr"`
	Offname string `xml:"OFFNAME,attr"`
	Shortname string `xml:"SHORTNAME,attr"`
	Aolevel string `xml:"AOLEVEL,attr"`
	Postalcode string `xml:"POSTALCODE,attr"`
	Actstatus string `xml:"ACTSTATUS,attr"`
}

type AddrObjects struct {
	Object []AddrObject
}

func(a AddrObject) GetXmlFile() string {
	return "AS_ADDROBJ_"
}


func (a AddrObject) TableName() string {
	return "fias_address"
}

func(a *AddrObject) Import(f os.FileInfo, wg *sync.WaitGroup)  {
	wg.Add(1)
	defer wg.Done()

	start := time.Now();
	path, err := filepath.Abs("upload/" + f.Name())
	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println("Error opening file: ", err)
	}
	defer xmlFile.Close()

	decoder := xml.NewDecoder(xmlFile)
	total := 0

	var element string
	var collection []interface{}
	db := config.Db

	for {
		t, _ := decoder.Token()
		if t == nil {
			break
		}
		switch se := t.(type) {
		case xml.StartElement:
			element = se.Name.Local
			if element == "Object" {

				decoder.DecodeElement(&a, &se)
				a.ID = 0
				//db.Create(&a)

				//fmt.Println(object.Formalname)
				if len(collection) < 2500 {
					collection = append(collection, *a)
					total++
				} else {
					err := BatchInsert(db, collection)
					if err != nil {
						fmt.Println("error", err.Error())
					}
					collection = collection[:0]
				}
			}
		}
	}
	if len(collection) > 0 {
		err := BatchInsert(db, collection)
		if err != nil {
			fmt.Println("error", err.Error())
		}
	}
	finish := time.Now()
	fmt.Println("Количество добавленных записей в адреса:", total)
	fmt.Println("Время выполнения адресов:", finish.Sub(start))
	fmt.Println(a.TableName(), f.Name())
}