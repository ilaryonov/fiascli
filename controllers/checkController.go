package controllers

import (
	"fmt"
	"fiascli/models"
	_ "net/http"
	_ "io/ioutil"
	"github.com/tiaguinho/gosoap"
	_ "log"
	"fiascli/config"
	"strconv"
	"log"
	_ "fiascli/helpers"
	"path/filepath"
	"os"
	"regexp"
	"sync"
	"fiascli/helpers"
)

type Method struct {
	GetAllDownloadFileInfo  string
	GetLastDownloadFileInfo string
}

var address models.AddrObject
var house models.HouseObject

func CheckUpdates() {
	db := config.Db

	version := &models.Option{}
	db.Where(&models.Option{Name: "version"}).First(&version)
	soapResult := GetAllDownloadFileInfo()

	//fmt.Println("key: ", lastVersion.VersionId, "link:",lastVersion.FiasCompleteXmlUrl)
	if len(version.Value) <= 0 {
		log.Printf("No result found.")
		lastVersion := soapResult[len(soapResult)-1]
		log.Println("Last => ", lastVersion)
	} else {
		//@TODO для теста беру последнюю дельту
		lastVersion := soapResult[len(soapResult)-1]
		log.Println(lastVersion.VersionId, lastVersion.FiasDeltaXmlUrl)
		//TODO FULL VERSION
		download(&lastVersion)

	}
}

type GetAllDownloadFileInfoResponse struct {
	GetAllDownloadFileInfoResult GetAllDownloadFileInfoResult
}

type GetAllDownloadFileInfoResult struct {
	DownloadFileInfo []DownloadFileInfo
}

type DownloadFileInfo struct {
	VersionId          string
	TextVersion        string
	FiasCompleteXmlUrl string
	FiasDeltaXmlUrl    string
}

func GetAllDownloadFileInfo() []DownloadFileInfo {

	soap, err := gosoap.SoapClient("https://fias.nalog.ru/WebServices/Public/DownloadService.asmx?WSDL")
	if err != nil {
		log.Printf("error not expected: %s", err)
	}

	params := gosoap.Params{"trace": "1"}

	response := GetAllDownloadFileInfoResponse{}

	err = soap.Call("GetAllDownloadFileInfo", params)
	if err != nil {
		log.Printf("error in soap call: %s", err)
	}
	soap.Unmarshal(&response)

	result := response.GetAllDownloadFileInfoResult.DownloadFileInfo

	return result
}

func VersionHandler() {
	db := config.Db

	version := &models.Option{}
	db.Where(&models.Option{Name: "version"}).First(&version)
	if len(version.Value) <= 0 {
		log.Printf("No result found.")
	} else {
		version, _ := strconv.ParseInt(version.Value, 0, 64)
		fmt.Println(version)
	}
}

func download(version *DownloadFileInfo) {
	var wg sync.WaitGroup
	file := helpers.FileData{version.VersionId, version.FiasDeltaXmlUrl}
	file.DownloadFile()

	filepath.Walk("upload/", func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			if r, err := regexp.MatchString(models.AddrObject{}.GetXmlFile(), f.Name()); err == nil && r {
				go address.Import(f, &wg)
			}
			if r, err := regexp.MatchString(models.HouseObject{}.GetXmlFile(), f.Name()); err == nil && r {
				go house.Import(f, &wg)
			}
		}
		return nil
	})
	wg.Wait()

	v := models.Option{Name: "version", Value: version.VersionId}

	optionVersion := &models.Option{}
	config.Db.First(&optionVersion)
	if optionVersion.Value != "" {
		config.Db.Model(optionVersion).Update(&v)
		log.Println("update")
	} else {
		config.Db.Model(optionVersion).Create(&v)
		log.Println("adddd")
	}

	os.RemoveAll("upload/")
	os.MkdirAll("upload/", 0777)
}
