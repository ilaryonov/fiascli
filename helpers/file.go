package helpers

import (
	"path"
	"log"
	"os"
	"net/http"
	"io"
	"github.com/mholt/archiver"
	"strconv"
	"time"
	"fmt"
)

type FileData struct {
	Version string
	Path    string
}

func (f *FileData) DownloadFile() {
	dest := "upload/"
	fileName := path.Base(f.Path)

	log.Printf("Downloading file: '%s' from: '%s'\n", fileName, f.Path)

	start := time.Now()

	out, err := os.Create(dest + fileName)
	if err != nil {
		log.Println(err.Error())
	}
	defer out.Close()

	headResp, err := http.Head(f.Path)

	if err != nil {
		panic(err)
	}

	defer headResp.Body.Close()

	size, err := strconv.Atoi(headResp.Header.Get("Content-Length"))

	if err != nil {
		log.Fatal(err.Error())
	}

	done := make(chan int64)

	go printPersent(done, dest+fileName, int64(size))

	resp, err := http.Get(f.Path)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer resp.Body.Close()

	w, err := io.Copy(out, resp.Body)
	if err != nil {
		log.Fatal(err.Error())
	}

	done <- w

	elapsed := time.Since(start)
	log.Printf("Download completed in %s", elapsed)

	log.Printf("Extracting file: '%s'", fileName)
	err = archiver.Rar.Open(dest+fileName, dest)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Printf("Remove archive file: '%s'", fileName)
	err = os.Remove(dest + fileName)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func printPersent(done chan int64, file string, total int64) {
	finish := false

	for {
		select {
		case <-done:
			finish = true
		default:
			file, err := os.Open(file)
			if err != nil {
				log.Fatal(err)
			}

			fi, err := file.Stat()
			if err != nil {
				log.Fatal(err)
			}

			size := fi.Size()

			if size == 0 {
				size = 1
			}

			percent := float64(size) / float64(total) * 100

			fmt.Printf("%.0f", percent)
			fmt.Println("%")
		}

		if finish {
			break
		}

		time.Sleep(time.Second * 5)
	}
}
