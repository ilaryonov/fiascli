package main

import (
	"github.com/codegangsta/cli"
	"os"
	"fiascli/controllers"
	"io/ioutil"
	"github.com/jinzhu/gorm"
	"flag"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-yaml/yaml"
	"fiascli/config"
	"fmt"
	"fiascli/models"
)

func main() {
	initDb()

	app := cli.NewApp()
	app.Name = "fiascli"
	app.Usage = "cli fias program"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:  "version",
			Usage: "fias version",
			Action: func(c *cli.Context) {
				controllers.VersionHandler()
			},
		},
		{
			Name:  "checkdelta",
			Usage: "check deltas from fias.nalog.ru",
			Action: func(c *cli.Context) {
				controllers.CheckUpdates()
			},
		},
	}

	app.Run(os.Args)
}

func initDb() {
	conf := config.Config{}
	path := flag.String("Config", "config/config.yaml", "config file path")
	data, err := ioutil.ReadFile(*path)
	if err != nil {
		panic(err.Error())
	}
	err = yaml.Unmarshal(data, &conf)
	if err != nil {
		panic(err.Error())
	}

	config.Db, err = gorm.Open("mysql", conf.Dsn)
	os.MkdirAll("upload/", 0777)
	//config.Db.LogMode(true)
	//defer config.Db.Close()

	if (err != nil) {
		fmt.Println(err.Error())
	}
	config.Db.AutoMigrate(&models.AddrObject{})
	config.Db.AutoMigrate(&models.HouseObject{})
	config.Db.AutoMigrate(&models.Option{})
}
