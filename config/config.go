package config

import (
	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	Dsn string `yaml:"dsn"`
}

var Db *gorm.DB
